<?php

use Illuminate\Database\Seeder;

class bookseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\books::insert([
        	[
        		'isbn' => '22431',
        		'title' => 'Head First HTML5',
        		'author' => 'Eric Freeman',
        		'publisher' => 'O Reilly',
        		'year' => 20110,
        		'price' => 52000,
        	],
        ]);
    }
}
