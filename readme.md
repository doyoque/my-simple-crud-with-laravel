## Setting Up before use

This is simple crud is build up by laravel. Before using this files better set up the database configuration at simplecrud/.env .

## How to use this

I've already create account for log in as administrator. The email is admin@gmail.com and the password is admin123. When you logged in you will be directed to the admin view. In that view you can create, update, and delete the data. Also the index view can access as guest and admin.