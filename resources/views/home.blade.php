@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row">
  @foreach ($book_list as $book)
  <div class="col-sm-3">
   <div class="panel panel-primary">
    <div class="panel-heading text-center">{{ $book -> title }}</div>
    <div class="panel-body">
     <p>No ISBN: {{ $book -> isbn }}</p>
     <p>Nama Pengarang: {{ $book -> author }}</p>
     <p>Penerbit: {{ $book -> publisher }}</p>
     <p>Tahun Terbit: {{ $book -> year }}</p>
     <p>Harga Buku: {{ $book -> price }}</p>
    </div>
    <div class="panel-footer"></div>
   </div>
  </div>
  @endforeach
 </div>
</div>
@endsection