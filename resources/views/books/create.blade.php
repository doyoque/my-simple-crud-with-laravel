@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">Tambah Buku</div>

				<form action="{{ URL('/store') }}" method="POST">
				<div class="panel-body">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="isbn">Masukan Nomor ISBN</label>
						<input type="text" class="form-control" name="isbn" required>
						<br>
						<label for="title">Masukan Judul Buku</label>
						<input type="text" class="form-control" name="title" required>
						<br>
						<label for="author">Masukan Nama Pengarang</label>
						<input type="text" class="form-control" name="author" required>
						<br>
						<label for="publisher">Masukan Nama Penerbit</label>
						<input type="text" class="form-control" name="publisher" required>
						<br>
						<label for="year">Masukan Tahun Terbit</label>
						<input type="text" class="form-control" name="year" required>
						<br>
						<label for="price">Masukan Harga Buku</label>
						<input type="text" class="form-control" name="price" required>
						<br>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-success" type="submit">Input Data</button>
					<button class="btn btn-danger" type="reset">Reset</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection