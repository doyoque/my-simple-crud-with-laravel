@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">Edit Data Buku</div>

				<form action="{{ URL('/store') }}" method="POST">
				<div class="panel-body">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="isbn">Ubah Nomor ISBN</label>
						<input type="text" class="form-control" value="{{ $showById->isbn }}" name="isbn" required>
						<br>
						<label for="title">Ubah Judul Buku</label>
						<input type="text" class="form-control" value="{{ $showById->title }}" name="title" required>
						<br>
						<label for="author">Ubah Nama Pengarang</label>
						<input type="text" class="form-control" value="{{ $showById->author }}" name="author" required>
						<br>
						<label for="publisher">Ubah Nama Penerbit</label>
						<input type="text" class="form-control" value="{{ $showById->publisher }}" name="publisher" required>
						<br>
						<label for="year">Ubah Tahun Terbit</label>
						<input type="text" class="form-control" value="{{ $showById->year }}" name="year" required>
						<br>
						<label for="price">Ubah Harga Buku</label>
						<input type="text" class="form-control" value="{{ $showById->price }}" name="price" required>
						<br>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-success" type="submit">Ubah Data</button>
					<button class="btn btn-danger" type="reset">Set default</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection