@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row">
  <div class="col-md-8 col-md-offset-2">
   <div class="panel panel-default">
    <div class="panel-heading"><a href="{{ URL('books/create') }}" class="btn btn-sm btn-primary">Tambah Buku</a></div>

    <div class="panel-body">
     <table class="table table-bordered table-hover">
      <thead>
       <tr>
        <th>ISBN</th>
        <th>Judul Buku</th>
        <th>Pengarang</th>
        <th>Penerbit</th>
        <th>Tahun Terbit</th>
        <th>Harga Buku</th>
        <th>Opsi</th>
       </tr>
      </thead>

      <tbody>
       @foreach ($book_list as $book)
       <tr>
        <td>{{ $book -> isbn }}</td>
        <td>{{ $book -> title }}</td>
        <td>{{ $book -> author }}</td>
        <td>{{ $book -> publisher }}</td>
        <td>{{ $book -> year }}</td>
        <td>{{ $book -> price }}</td>
        <td>
         <a href="{{ URL('books/edit') }}/{{ $book -> id }}" class="btn btn-sm btn-primary">Edit</a>
         <a href="{{ URL('/delete') }}/{{ $book -> id }}" class="btn btn-sm btn-danger">Delete</a>
        </td>
       </tr>
       @endforeach
      </tbody>
     </table>
    </div>

    <div class="panel-footer">
     <a href="{{ URL('books/create') }}" class="btn btn-sm btn-primary">Tambah Buku</a>
    </div>

   </div>
  </div>
 </div>
</div>
@endsection