<?php

namespace App\Http\Controllers;

use \App\Books;
use Illuminate\Http\Request;

class admin extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin()
    {
        $book_list = Books::all();
        return view('/admin', compact('book_list'));
    }
}
