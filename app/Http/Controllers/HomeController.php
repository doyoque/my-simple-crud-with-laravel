<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use \App\Books;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $book_list = Books::all();
        return view('home', compact('book_list'));
    }

    public function create()
    {
        $book_list = Books::all();
        return view('books.create', compact('book_list'));
    }

    public function store(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'isbn' => 'required',
            'title' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'year' => 'required',
            'price' => 'required',
        ]);

        $data = [
            'isbn' => $request -> isbn,
            'title' => $request -> title,
            'author' => $request -> author,
            'publisher' => $request -> publisher,
            'year' => $request -> year,
            'price' => $request -> price,
        ];

        $store = Books::insert($data);
        return redirect()->route('admin', compact('store'));
    }

    public function edit($id)
    {
        $showById = Books::find($id);
        return view('books.edit', compact('showById'));
    }

    public function update(Request $request, $id)
    {
        $validasi = Validator::make($request->all(), [
            'isbn' => 'required',
            'title' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'year' => 'required',
            'price' => 'required',
        ]);

        $data = [
            'isbn' => $request -> isbn,
            'title' => $request -> title,
            'author' => $request -> author,
            'publisher' => $request -> publisher,
            'year' => $request -> year,
            'price' => $request -> price
        ];

        $update = Books::where('id', $id)->update($data);
        return redirect()->route('admin', compact('update'));
    }

    public function delete($id)
    {
        $showById = Books::where('id', $id)->delete();
        return redirect()->route('admin', compact('showById'));
    }
}
